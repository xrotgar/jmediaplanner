package ru.xrotgar.mediaplanner.repository;

import org.springframework.data.repository.CrudRepository;
import ru.xrotgar.mediaplanner.domain.MediaplanEntity;

public interface MediaplanRepository extends CrudRepository<MediaplanEntity, Long> {

}
