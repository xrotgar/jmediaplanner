package ru.xrotgar.mediaplanner.repository;

import org.springframework.data.repository.CrudRepository;
import ru.xrotgar.mediaplanner.domain.AdvertisingCampaignEntity;

public interface AdvertisingCampaignRepository extends CrudRepository<AdvertisingCampaignEntity, Long> {

}
