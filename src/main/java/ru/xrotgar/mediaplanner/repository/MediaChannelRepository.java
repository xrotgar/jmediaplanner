package ru.xrotgar.mediaplanner.repository;

import org.springframework.data.repository.CrudRepository;
import ru.xrotgar.mediaplanner.domain.MediaChannelEntity;

public interface MediaChannelRepository extends CrudRepository<MediaChannelEntity, Long> {

}
