package ru.xrotgar.mediaplanner.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ru.xrotgar.mediaplanner.domain.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Long> {

	@Query("SELECT DISTINCT user FROM UserEntity user WHERE user.login = :login")
	public UserEntity findByLogin(@Param("login")String login);

	@Query("SELECT DISTINCT user FROM UserEntity user WHERE user.email = :email")
	public UserEntity findByEmail(@Param("email") String email);
}
