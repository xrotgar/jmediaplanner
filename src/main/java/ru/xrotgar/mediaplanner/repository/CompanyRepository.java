package ru.xrotgar.mediaplanner.repository;

import org.springframework.data.repository.CrudRepository;
import ru.xrotgar.mediaplanner.domain.CompanyEntity;

public interface CompanyRepository extends CrudRepository<CompanyEntity, Long>{

}
