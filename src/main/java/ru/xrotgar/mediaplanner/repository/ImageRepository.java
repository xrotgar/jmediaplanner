package ru.xrotgar.mediaplanner.repository;

import org.springframework.data.repository.CrudRepository;
import ru.xrotgar.mediaplanner.domain.ImageEntity;

public interface ImageRepository extends CrudRepository<ImageEntity, Long> {

}