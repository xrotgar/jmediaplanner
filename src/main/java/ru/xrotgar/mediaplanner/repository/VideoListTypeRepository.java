package ru.xrotgar.mediaplanner.repository;

import org.springframework.data.repository.CrudRepository;
import ru.xrotgar.mediaplanner.domain.VideoListTypeEntity;

public interface VideoListTypeRepository extends CrudRepository<VideoListTypeEntity, String> {

}