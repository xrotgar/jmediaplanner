package ru.xrotgar.mediaplanner.repository;

import org.springframework.data.repository.CrudRepository;
import ru.xrotgar.mediaplanner.domain.VideoResourceEntity;

public interface VideoResourceRepository extends CrudRepository<VideoResourceEntity, Long> {

}
