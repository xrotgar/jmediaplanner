package ru.xrotgar.mediaplanner.repository;

import org.springframework.data.repository.CrudRepository;
import ru.xrotgar.mediaplanner.domain.ItemEntity;

public interface ItemRepository extends CrudRepository<ItemEntity, Long> {

}
