package ru.xrotgar.mediaplanner.repository;

import org.springframework.data.repository.CrudRepository;
import ru.xrotgar.mediaplanner.domain.ImageResourceEntity;

public interface ImageResourceRepository extends CrudRepository<ImageResourceEntity, Long> {

}
