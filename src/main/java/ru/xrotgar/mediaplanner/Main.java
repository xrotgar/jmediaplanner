package ru.xrotgar.mediaplanner;

import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import ru.xrotgar.mediaplanner.domain.CompanyEntity;
import ru.xrotgar.mediaplanner.service.CompanyService;

public class Main {
	public static void main(String[] args) {
		System.out.println("");
		GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
		ctx.load("classpath:META-INF/spring/app-context.xml");
		ctx.refresh();

		AuthenticationManager am = ctx.getBean("authenticationManager", AuthenticationManager.class);
		try {
			Authentication request = new UsernamePasswordAuthenticationToken("e.samarin", "qwerty");
			Authentication result = am.authenticate(request);
			SecurityContextHolder.getContext().setAuthentication(result);
			String username = (String)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			System.out.println(username);

//			CompanyService service = ctx.getBean("jpaCompanyService", CompanyService.class);
//			CompanyEntity company = new CompanyEntity();
//			company.setName("iMiDiTi");
//			service.save(company);
		}
		catch(AuthenticationException e) {
			System.out.println("Authentication failed: " + e.getMessage());
		}
	}

}
