package ru.xrotgar.mediaplanner.service;

import ru.xrotgar.mediaplanner.domain.VideoResourceEntity;

public interface VideoResourceService extends BaseService <VideoResourceEntity, Long> {

}
