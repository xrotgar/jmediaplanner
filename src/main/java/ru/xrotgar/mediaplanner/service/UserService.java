package ru.xrotgar.mediaplanner.service;

import ru.xrotgar.mediaplanner.domain.UserEntity;

public interface UserService extends BaseService <UserEntity, Long> {
	public UserEntity findByLogin(String login);
	public UserEntity findByEmail(String email);
}
