package ru.xrotgar.mediaplanner.service;

import ru.xrotgar.mediaplanner.domain.ItemEntity;

public interface ItemService extends BaseService <ItemEntity, Long> {

}
