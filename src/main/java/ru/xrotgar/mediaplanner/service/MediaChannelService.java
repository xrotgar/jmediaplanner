package ru.xrotgar.mediaplanner.service;

import ru.xrotgar.mediaplanner.domain.MediaChannelEntity;

public interface MediaChannelService extends BaseService <MediaChannelEntity, Long> {

}
