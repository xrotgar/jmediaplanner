package ru.xrotgar.mediaplanner.service.impl;

import com.google.common.collect.Lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.xrotgar.mediaplanner.domain.ItemEntity;
import ru.xrotgar.mediaplanner.repository.ItemRepository;
import ru.xrotgar.mediaplanner.service.ItemService;

import java.util.List;

@Service("jpaItemService")
@Transactional
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemRepository repository;

	@Transactional(readOnly = true)
	public List<ItemEntity> findAll() {
		return Lists.newArrayList(this.repository.findAll());
	}

	@Transactional(readOnly = true)
	public ItemEntity findById(Long id) {
		return this.repository.findOne(id);
	}

	public ItemEntity save(ItemEntity item) {
		return this.repository.save(item);
	}

	public void delete(ItemEntity item) {
		this.repository.delete(item);
	}

	public void delete(Long id) {
		this.repository.delete(id);
	}
}
