package ru.xrotgar.mediaplanner.service.impl;

import com.google.common.collect.Lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.xrotgar.mediaplanner.domain.MediaChannelEntity;
import ru.xrotgar.mediaplanner.repository.MediaChannelRepository;
import ru.xrotgar.mediaplanner.service.MediaChannelService;

import java.util.List;

@Service("jpaMediaChannelService")
@Transactional
public class MediaChannelServiceImpl implements MediaChannelService {

	@Autowired
	private MediaChannelRepository repository;

	@Transactional(readOnly = true)
	public List<MediaChannelEntity> findAll() {
		return Lists.newArrayList(this.repository.findAll());
	}

	@Transactional(readOnly = true)
	public MediaChannelEntity findById(Long id) {
		return this.repository.findOne(id);
	}

	public MediaChannelEntity save(MediaChannelEntity mediaChannel) {
		return this.repository.save(mediaChannel);
	}

	public void delete(MediaChannelEntity mediaChannel) {
		this.repository.delete(mediaChannel);
	}

	public void delete(Long id) {
		this.repository.delete(id);
	}
}
