package ru.xrotgar.mediaplanner.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import ru.xrotgar.mediaplanner.domain.AdvertisingCampaignEntity;
import ru.xrotgar.mediaplanner.repository.AdvertisingCampaignRepository;
import ru.xrotgar.mediaplanner.service.AdvertisingCampaignService;

@Service("jpaAdvertisingCampaignService")
@Transactional
public class AdvertisingCampaignServiceImpl implements AdvertisingCampaignService {

	@Autowired
	private AdvertisingCampaignRepository repository;
	
	@Transactional(readOnly = true)
	public List<AdvertisingCampaignEntity> findAll() {
		return Lists.newArrayList(this.repository.findAll());
	}

	@Transactional(readOnly = true)
	public AdvertisingCampaignEntity findById(Long id) {
		return this.repository.findOne(id);
	}

	public AdvertisingCampaignEntity save(AdvertisingCampaignEntity advertisingCampaign) {
		return this.repository.save(advertisingCampaign);
	}

	public void delete(AdvertisingCampaignEntity advertisingCampaign) {
		this.repository.delete(advertisingCampaign);
	}

	public void delete(Long id) {
		this.repository.delete(id);
	}
}
