package ru.xrotgar.mediaplanner.service.impl;

import com.google.common.collect.Lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.xrotgar.mediaplanner.domain.MediaplanEntity;
import ru.xrotgar.mediaplanner.repository.MediaplanRepository;
import ru.xrotgar.mediaplanner.service.MediaplanService;

import java.util.List;

@Service("jpaMediaplanService")
@Transactional
public class MediaplanServiceImpl implements MediaplanService {

	@Autowired
	private MediaplanRepository repository;

	@Transactional(readOnly = true)
	public List<MediaplanEntity> findAll() {
		return Lists.newArrayList(this.repository.findAll());
	}

	@Transactional(readOnly = true)
	public MediaplanEntity findById(Long id) {
		return this.repository.findOne(id);
	}

	public MediaplanEntity save(MediaplanEntity mediaplan) {
		return this.repository.save(mediaplan);
	}

	public void delete(MediaplanEntity mediaplan) {
		this.repository.delete(mediaplan);
	}

	public void delete(Long id) {
		this.repository.delete(id);
	}
}
