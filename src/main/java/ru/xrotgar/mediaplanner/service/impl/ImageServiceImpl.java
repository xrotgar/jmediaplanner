package ru.xrotgar.mediaplanner.service.impl;

import com.google.common.collect.Lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.xrotgar.mediaplanner.domain.ImageEntity;
import ru.xrotgar.mediaplanner.repository.ImageRepository;
import ru.xrotgar.mediaplanner.service.ImageService;

import java.util.List;

@Service("jpaImageService")
@Transactional
public class ImageServiceImpl implements ImageService {

	@Autowired
	private ImageRepository repository;

	@Transactional(readOnly = true)
	public List<ImageEntity> findAll() {
		return Lists.newArrayList(this.repository.findAll());
	}

	@Transactional(readOnly = true)
	public ImageEntity findById(Long id) {
		return this.repository.findOne(id);
	}

	public ImageEntity save(ImageEntity image) {
		return this.repository.save(image);
	}

	public void delete(ImageEntity image) {
		this.repository.delete(image);
	}

	public void delete(Long id) {
		this.repository.delete(id);
	}
}
