package ru.xrotgar.mediaplanner.service.impl;

import com.google.common.collect.Lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.xrotgar.mediaplanner.domain.VideoListTypeEntity;
import ru.xrotgar.mediaplanner.repository.VideoListTypeRepository;
import ru.xrotgar.mediaplanner.service.VideoListTypeService;

import java.util.List;

@Service("jpaVideoListTypeService")
@Transactional
public class VideoListTypeServiceImpl implements VideoListTypeService {

	@Autowired
	private VideoListTypeRepository repository;

	@Transactional(readOnly = true)
	public List<VideoListTypeEntity> findAll() {
		return Lists.newArrayList(this.repository.findAll());
	}

	@Transactional(readOnly = true)
	public VideoListTypeEntity findById(String id) {
		return this.repository.findOne(id);
	}

	public VideoListTypeEntity save(VideoListTypeEntity videoListType) {
		return this.repository.save(videoListType);
	}

	public void delete(VideoListTypeEntity videoListType) {
		this.repository.delete(videoListType);
	}

	public void delete(String id) {
		this.repository.delete(id);
	}
}
