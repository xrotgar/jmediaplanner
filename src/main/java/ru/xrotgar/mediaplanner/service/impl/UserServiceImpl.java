package ru.xrotgar.mediaplanner.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import ru.xrotgar.mediaplanner.domain.UserEntity;
import ru.xrotgar.mediaplanner.repository.UserRepository;
import ru.xrotgar.mediaplanner.service.UserService;

@Service("jpaUserService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository repository;
	
	@Transactional(readOnly = true)
	public List<UserEntity> findAll() {
		return Lists.newArrayList(this.repository.findAll());
	}

	@Transactional(readOnly = true)
	public UserEntity findById(Long id) {
		return this.repository.findOne(id);
	}

	public UserEntity save(UserEntity user) {
		return this.repository.save(user);
	}

	public void delete(UserEntity user) {
		this.repository.delete(user);
	}

	public void delete(Long id) {
		this.repository.delete(id);
	}

	@Transactional(readOnly = true)
	public UserEntity findByLogin(String login) {
		return this.repository.findByLogin(login);
	}

	@Transactional(readOnly = true)
	public UserEntity findByEmail(String email) {
		return this.repository.findByEmail(email);
	}
}
