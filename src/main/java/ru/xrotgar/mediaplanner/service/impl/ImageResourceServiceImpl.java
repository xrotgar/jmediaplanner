package ru.xrotgar.mediaplanner.service.impl;

import com.google.common.collect.Lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.xrotgar.mediaplanner.domain.ImageResourceEntity;
import ru.xrotgar.mediaplanner.repository.ImageResourceRepository;
import ru.xrotgar.mediaplanner.service.ImageResourceService;

import java.util.List;

@Service("jpaImageResourceService")
@Transactional
public class ImageResourceServiceImpl implements ImageResourceService {

	@Autowired
	private ImageResourceRepository repository;

	@Transactional(readOnly = true)
	public List<ImageResourceEntity> findAll() {
		return Lists.newArrayList(this.repository.findAll());
	}

	@Transactional(readOnly = true)
	public ImageResourceEntity findById(Long id) {
		return this.repository.findOne(id);
	}

	public ImageResourceEntity save(ImageResourceEntity imageResource) {
		return this.repository.save(imageResource);
	}

	public void delete(ImageResourceEntity imageResource) {
		this.repository.delete(imageResource);
	}

	public void delete(Long id) {
		this.repository.delete(id);
	}
}
