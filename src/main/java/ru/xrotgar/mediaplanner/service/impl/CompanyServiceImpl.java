package ru.xrotgar.mediaplanner.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import ru.xrotgar.mediaplanner.domain.CompanyEntity;
import ru.xrotgar.mediaplanner.repository.CompanyRepository;
import ru.xrotgar.mediaplanner.service.CompanyService;

@Service("jpaCompanyService")
@Transactional
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyRepository repository;
	
	@Transactional(readOnly = true)
	public List<CompanyEntity> findAll() {
		return Lists.newArrayList(this.repository.findAll());
	}

	@Transactional(readOnly = true)
	public CompanyEntity findById(Long id) {
		return this.repository.findOne(id);
	}

	public CompanyEntity save(CompanyEntity company) {
		return this.repository.save(company);
	}

	public void delete(CompanyEntity company) {
		this.repository.delete(company);
	}

	public void delete(Long id) {
		this.repository.delete(id);
	}
}
