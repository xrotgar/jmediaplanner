package ru.xrotgar.mediaplanner.service.impl;

import com.google.common.collect.Lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.xrotgar.mediaplanner.domain.VideoResourceEntity;
import ru.xrotgar.mediaplanner.repository.VideoResourceRepository;
import ru.xrotgar.mediaplanner.service.VideoResourceService;

import java.util.List;

@Service("jpaVideoResourceService")
@Transactional
public class VideoResourceServiceImpl implements VideoResourceService {

	@Autowired
	private VideoResourceRepository repository;

	@Transactional(readOnly = true)
	public List<VideoResourceEntity> findAll() {
		return Lists.newArrayList(this.repository.findAll());
	}

	@Transactional(readOnly = true)
	public VideoResourceEntity findById(Long id) {
		return this.repository.findOne(id);
	}

	public VideoResourceEntity save(VideoResourceEntity videoResource) {
		return this.repository.save(videoResource);
	}

	public void delete(VideoResourceEntity videoResource) {
		this.repository.delete(videoResource);
	}

	public void delete(Long id) {
		this.repository.delete(id);
	}
}
