package ru.xrotgar.mediaplanner.service;

import ru.xrotgar.mediaplanner.domain.VideoListTypeEntity;

public interface VideoListTypeService extends BaseService <VideoListTypeEntity, String> {

}
