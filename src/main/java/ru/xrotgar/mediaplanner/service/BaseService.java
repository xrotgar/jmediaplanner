package ru.xrotgar.mediaplanner.service;

import java.io.Serializable;
import java.util.List;

public interface BaseService <T, Id extends Serializable> {
	public List<T> findAll();
	public T findById(Id id);
	public T save(T entity);
	public void delete(T entity);
	public void delete(Id id);
}
