package ru.xrotgar.mediaplanner.service;

import ru.xrotgar.mediaplanner.domain.AdvertisingCampaignEntity;

public interface AdvertisingCampaignService extends BaseService <AdvertisingCampaignEntity, Long> {

}
