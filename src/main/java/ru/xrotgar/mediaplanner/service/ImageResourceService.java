package ru.xrotgar.mediaplanner.service;

import ru.xrotgar.mediaplanner.domain.ImageResourceEntity;

public interface ImageResourceService extends BaseService <ImageResourceEntity, Long> {

}
