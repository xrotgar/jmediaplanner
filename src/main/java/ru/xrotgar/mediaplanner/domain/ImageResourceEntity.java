package ru.xrotgar.mediaplanner.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name = "image_resource", schema = "public", catalog = "mediaplanner")
public class ImageResourceEntity extends AbstractResourceEntity implements Serializable {

	private Collection<ImageEntity> images;

	@OneToMany(mappedBy = "imageResource", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Collection<ImageEntity> getImages() {
		return images;
	}

	public void setImages(Collection<ImageEntity> images) {
		this.images = images;
	}
}
