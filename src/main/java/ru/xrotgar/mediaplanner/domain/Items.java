package ru.xrotgar.mediaplanner.domain;

import java.io.Serializable;
import java.util.List;

public class Items implements Serializable {

	private List<ItemEntity> items;

	public Items() { }

	public Items(List<ItemEntity> items) {
		this.items = items;
	}

	public List<ItemEntity> getItems() {
		return items;
	}

	public void setItems(List<ItemEntity> items) {
		this.items = items;
	}
}
