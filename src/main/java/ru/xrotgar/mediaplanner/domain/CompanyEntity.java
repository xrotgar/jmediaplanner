package ru.xrotgar.mediaplanner.domain;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Auditable;

@Entity
@Table(name = "company", schema = "public", catalog = "mediaplanner")
public class CompanyEntity implements Auditable<String,Long>, Serializable {

	private Long id;
	private String name;

	private String createdBy;
	private DateTime createdDate;
	private String lastModifiedBy;
	private DateTime lastModifiedDate;

	private Collection<UserEntity> users;
	private Collection<AdvertisingCampaignEntity> advertisingCampaigns;
	private Collection<MediaChannelEntity> mediaChannels;
	private Collection<VideoResourceEntity> videoResources;
	private Collection<ImageResourceEntity> imageResources;

	@Id
	@SequenceGenerator(name = "seq_company", sequenceName = "seq_company", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_company")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "name", length = 128, nullable = false)
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@CreatedBy
	@Column(name = "created_by", length = 128)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@CreatedDate
	@Column(name = "created_date")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}

	@LastModifiedBy
	@Column(name = "last_modified_by", length = 128)
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	@LastModifiedDate
	@Column(name = "last_modified_date")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(DateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	@ManyToMany
	@JoinTable(name = "users_companies", joinColumns = @JoinColumn(name = "company_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
	public Collection<UserEntity> getUsers() {
		return users;
	}

	public void setUsers(Collection<UserEntity> users) {
		this.users = users;
	}

	@OneToMany(mappedBy = "advertiser", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Collection<AdvertisingCampaignEntity> getAdvertisingCampaigns() {
		return advertisingCampaigns;
	}

	public void setAdvertisingCampaigns(Collection<AdvertisingCampaignEntity> advertisingCampaigns) {
		this.advertisingCampaigns = advertisingCampaigns;
	}

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Collection<MediaChannelEntity> getMediaChannels() {
		return mediaChannels;
	}

	public void setMediaChannels(Collection<MediaChannelEntity> mediaChannels) {
		this.mediaChannels = mediaChannels;
	}

	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Collection<VideoResourceEntity> getVideoResources() {
		return videoResources;
	}

	public void setVideoResources(Collection<VideoResourceEntity> videoResources) {
		this.videoResources = videoResources;
	}

	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Collection<ImageResourceEntity> getImageResources() {
		return imageResources;
	}

	public void setImageResources(Collection<ImageResourceEntity> imageResources) {
		this.imageResources = imageResources;
	}

	@JsonIgnore
	@Transient
	public boolean isNew() {
		if(this.id == null) {
			return true;
		}
		else {
			return false;
		}
	}
}
