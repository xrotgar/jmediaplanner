package ru.xrotgar.mediaplanner.domain;

import java.io.Serializable;
import java.util.List;

public class VideoListTypes implements Serializable {

	private List<VideoListTypeEntity> videoListTypes;

	public VideoListTypes() { }

	public VideoListTypes(List<VideoListTypeEntity> videoListTypes) {
		this.videoListTypes = videoListTypes;
	}

	public List<VideoListTypeEntity> getVideoListTypes() {
		return videoListTypes;
	}

	public void setVideoListTypes(List<VideoListTypeEntity> videoListTypes) {
		this.videoListTypes = videoListTypes;
	}
}
