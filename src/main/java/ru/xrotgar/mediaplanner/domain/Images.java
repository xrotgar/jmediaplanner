package ru.xrotgar.mediaplanner.domain;

import java.io.Serializable;
import java.util.List;

public class Images implements Serializable {

	private List<ImageEntity> images;

	public Images() { }

	public Images(List<ImageEntity> images) {
		this.images = images;
	}

	public List<ImageEntity> getImages() {
		return images;
	}

	public void setImages(List<ImageEntity> images) {
		this.images = images;
	}
}
