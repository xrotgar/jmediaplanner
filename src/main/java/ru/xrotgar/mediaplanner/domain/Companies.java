package ru.xrotgar.mediaplanner.domain;

import java.io.Serializable;
import java.util.List;

public class Companies implements Serializable {

	private List<CompanyEntity> companies;

	public Companies() { }

	public Companies(List<CompanyEntity> companies) {
		this.companies = companies;
	}

	public List<CompanyEntity> getCompanies() {
		return companies;
	}

	public void setCompanies(List<CompanyEntity> companies) {
		this.companies = companies;
	}
}
