package ru.xrotgar.mediaplanner.domain;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Auditable;

@Entity
@Table(name = "advertising_campaign", schema = "public", catalog = "mediaplanner")
public class AdvertisingCampaignEntity implements Auditable<String, Long>,Serializable {
	
	private Long id;
	private String name;

	private String createdBy;
	private DateTime createdDate;
	private String lastModifiedBy;
	private DateTime lastModifiedDate;

	private CompanyEntity advertiser;
	private Collection<MediaplanEntity> mediaplans;

	@Id
	@SequenceGenerator(name = "seq_advertising_campaign", sequenceName = "seq_advertising_campaign", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_advertising_campaign")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", length = 128, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@CreatedBy
	@Column(name = "created_by", length = 128)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@CreatedDate
	@Column(name = "created_date")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(DateTime createDate) {
		this.createdDate = createDate;
	}

	@LastModifiedBy
	@Column(name = "last_modified_by", length = 128)
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	@LastModifiedDate
	@Column(name = "last_modified_date")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(DateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	@ManyToOne
	@JoinColumn(name = "advertiser", nullable = false)
	public CompanyEntity getAdvertiser() {
		return advertiser;
	}

	public void setAdvertiser(CompanyEntity advertiser) {
		this.advertiser = advertiser;
	}

	@OneToMany(mappedBy = "advertisingCampaign", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Collection<MediaplanEntity> getMediaplans() {
		return mediaplans;
	}

	public void setMediaplans(Collection<MediaplanEntity> mediaplans) {
		this.mediaplans = mediaplans;
	}

	@JsonIgnore
	@Transient
	public boolean isNew() {
		if(this.id == null) {
			return true;
		}
		else {
			return false;
		}
	}
}
