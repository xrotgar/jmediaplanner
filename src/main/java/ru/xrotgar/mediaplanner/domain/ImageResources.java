package ru.xrotgar.mediaplanner.domain;

import java.io.Serializable;
import java.util.List;

public class ImageResources implements Serializable {

	private List<ImageResourceEntity> imageResources;

	public ImageResources() { }

	public ImageResources(List<ImageResourceEntity> imageResources) {
		this.imageResources = imageResources;
	}

	public List<ImageResourceEntity> getImageResources() {
		return imageResources;
	}

	public void setImageResources(List<ImageResourceEntity> imageResources) {
		this.imageResources = imageResources;
	}
}
