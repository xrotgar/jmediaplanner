package ru.xrotgar.mediaplanner.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Auditable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name = "media_channel", schema = "public", catalog = "mediaplanner")
public class MediaChannelEntity implements Auditable<String, Long>, Serializable {

	private Long id;
	private String name;
	private Boolean visible;

	private String createdBy;
	private DateTime createdDate;
	private String lastModifiedBy;
	private DateTime lastModifiedDate;

	private CompanyEntity company;
	private Collection<MediaplanEntity> mediaplans;

	@Id
	@SequenceGenerator(name = "seq_media_channel", sequenceName = "seq_media_channel", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_media_channel")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", length = 320, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "visible", nullable = false)
	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	@CreatedBy
	@Column(name = "created_by", length = 128)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@CreatedDate
	@Column(name = "created_date")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}

	@LastModifiedBy
	@Column(name = "last_modified_by", length = 128)
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	@LastModifiedDate
	@Column(name = "last_modified_date")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(DateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	@ManyToOne
	@JoinColumn(name = "company", nullable = false)
	public CompanyEntity getCompany() {
		return company;
	}

	public void setCompany(CompanyEntity company) {
		this.company = company;
	}

	@ManyToMany
	@JoinTable(name = "mediaplan_channel", joinColumns = @JoinColumn(name = "media_channel_id"), inverseJoinColumns = @JoinColumn(name = "mediaplan_id"))
	public Collection<MediaplanEntity> getMediaplans() {
		return mediaplans;
	}

	public void setMediaplans(Collection<MediaplanEntity> mediaplans) {
		this.mediaplans = mediaplans;
	}

	@JsonIgnore
	@Transient
	public boolean isNew() {
		if(this.id == null) {
			return true;
		}
		return false;
	}
}
