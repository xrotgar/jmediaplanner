package ru.xrotgar.mediaplanner.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name = "video_resource", schema = "public", catalog = "mediaplanner")
public class VideoResourceEntity extends AbstractResourceEntity implements Serializable {

	private Integer duration;
	private Collection<ItemEntity> items;

	@Column(name = "duration", nullable = false)
	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	@OneToMany(mappedBy = "videoResource", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Collection<ItemEntity> getItems() {
		return items;
	}

	public void setItems(Collection<ItemEntity> items) {
		this.items = items;
	}
}
