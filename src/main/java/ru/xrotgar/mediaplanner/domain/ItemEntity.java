package ru.xrotgar.mediaplanner.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Auditable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name = "item", schema = "public", catalog = "mediaplanner")
public class ItemEntity implements Auditable<String, Long>, Serializable {

	private Long id;
	private DateTime startDate;
	private DateTime endDate;
	private Integer startHour;
	private Integer startMin;
	private Integer startDayOfWeek;
	private Integer endHour;
	private Integer endMin;
	private Integer endDayOfWeek;

	private String createdBy;
	private DateTime createdDate;
	private String lastModifiedBy;
	private DateTime lastModifiedDate;

	private MediaplanEntity mediaplan;
	private Collection<ImageEntity> images;
	private VideoResourceEntity videoResource;

	@Id
	@SequenceGenerator(name = "seq_item", sequenceName = "seq_item", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_item")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "start_date", nullable = false)
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(DateTime startDate) {
		this.startDate = startDate;
	}

	@Column(name = "end_date", nullable = false)
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(DateTime endDate) {
		this.endDate = endDate;
	}

	@Column(name = "start_hour")
	public Integer getStartHour() {
		return startHour;
	}

	public void setStartHour(Integer startHour) {
		this.startHour = startHour;
	}

	@Column(name = "start_min")
	public Integer getStartMin() {
		return startMin;
	}

	public void setStartMin(Integer startMin) {
		this.startMin = startMin;
	}

	@Column(name = "start_day_of_week")
	public Integer getStartDayOfWeek() {
		return startDayOfWeek;
	}

	public void setStartDayOfWeek(Integer startDayOfWeek) {
		this.startDayOfWeek = startDayOfWeek;
	}

	@Column(name = "end_hour")
	public Integer getEndHour() {
		return endHour;
	}

	public void setEndHour(Integer endHour) {
		this.endHour = endHour;
	}

	@Column(name = "end_min")
	public Integer getEndMin() {
		return endMin;
	}

	public void setEndMin(Integer endMin) {
		this.endMin = endMin;
	}

	@Column(name = "end_day_of_week")
	public Integer getEndDayOfWeek() {
		return endDayOfWeek;
	}

	public void setEndDayOfWeek(Integer endDayOfWeek) {
		this.endDayOfWeek = endDayOfWeek;
	}

	@CreatedBy
	@Column(name = "created_by", length = 128)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@CreatedDate
	@Column(name = "created_date")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}

	@LastModifiedBy
	@Column(name = "last_modified_by", length = 128)
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	@LastModifiedDate
	@Column(name = "last_modified_date")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(DateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	@ManyToOne
	@JoinColumn(name = "mediaplan", nullable = false)
	public MediaplanEntity getMediaplan() {
		return mediaplan;
	}

	public void setMediaplan(MediaplanEntity mediaplan) {
		this.mediaplan = mediaplan;
	}

	@OneToMany(mappedBy = "item", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Collection<ImageEntity> getImages() {
		return images;
	}

	public void setImages(Collection<ImageEntity> images) {
		this.images = images;
	}

	@ManyToOne
	@JoinColumn(name = "videoResource", nullable = false)
	public VideoResourceEntity getVideoResource() {
		return videoResource;
	}

	public void setVideoResource(VideoResourceEntity videoResource) {
		this.videoResource = videoResource;
	}

	@JsonIgnore
	@Transient
	public boolean isNew() {
		if(this.id == null) {
			return true;
		}
		return false;
	}
}
