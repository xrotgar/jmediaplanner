package ru.xrotgar.mediaplanner.domain;

import java.io.Serializable;
import java.util.List;

public class MediaChannels implements Serializable {

	private List<MediaChannelEntity> mediaChannels;

	public MediaChannels() { }

	public MediaChannels(List<MediaChannelEntity> mediaChannels) {
		this.mediaChannels = mediaChannels;
	}

	public List<MediaChannelEntity> getMediaChannels() {
		return mediaChannels;
	}

	public void setMediaChannels(List<MediaChannelEntity> mediaChannels) {
		this.mediaChannels = mediaChannels;
	}
}
