package ru.xrotgar.mediaplanner.domain;

import java.io.Serializable;
import java.util.List;

public class Users implements Serializable {

	private List<UserEntity> users;

	public Users() { }

	public Users(List<UserEntity> users) {
		this.users = users;
	}

	public List<UserEntity> getUsers() {
		return users;
	}

	public void setUsers(List<UserEntity> users) {
		this.users = users;
	}
}
