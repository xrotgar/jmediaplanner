package ru.xrotgar.mediaplanner.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name = "video_list_type", schema = "public", catalog = "mediaplanner")
public class VideoListTypeEntity implements Serializable {

	private String name;
	private String description;

	private Collection<MediaplanEntity> mediaplans;

	@Id
	@Column(name = "name", length = 128, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description", columnDefinition = "TEXT")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(mappedBy = "videoListType", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Collection<MediaplanEntity> getMediaplans() {
		return mediaplans;
	}

	public void setMediaplans(Collection<MediaplanEntity> mediaplans) {
		this.mediaplans = mediaplans;
	}
}
