package ru.xrotgar.mediaplanner.domain;

import java.io.Serializable;
import java.util.List;

public class Mediaplans implements Serializable {

	private List<MediaplanEntity> mediaplans;

	public Mediaplans() { }

	public Mediaplans(List<MediaplanEntity> mediaplans) {
		this.mediaplans = mediaplans;
	}

	public List<MediaplanEntity> getMediaplans() {
		return mediaplans;
	}

	public void setMediaplans(List<MediaplanEntity> mediaplans) {
		this.mediaplans = mediaplans;
	}
}
