package ru.xrotgar.mediaplanner.domain;

import java.io.Serializable;
import java.util.List;

public class AdvertisingCampaigns implements Serializable {

	private List<AdvertisingCampaignEntity> advertisingCampaigns;

	public AdvertisingCampaigns() { }

	public AdvertisingCampaigns(List<AdvertisingCampaignEntity> advertisingCampaigns) {
		this.advertisingCampaigns = advertisingCampaigns;
	}

	public List<AdvertisingCampaignEntity> getAdvertisingCampaigns() {
		return advertisingCampaigns;
	}

	public void setAdvertisingCampaigns(List<AdvertisingCampaignEntity> advertisingCampaigns) {
		this.advertisingCampaigns = advertisingCampaigns;
	}
}
