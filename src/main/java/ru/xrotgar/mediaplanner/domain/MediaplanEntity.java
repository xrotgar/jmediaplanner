package ru.xrotgar.mediaplanner.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Auditable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name = "mediaplan", schema = "public", catalog = "mediaplanner")
public class MediaplanEntity implements Auditable<String, Long>, Serializable {

	private Long id;
	private String name;
	private DateTime startDate;
	private DateTime endDate;
	private DateTime paymentDate;

	private String createdBy;
	private DateTime createdDate;
	private String lastModifiedBy;
	private DateTime lastModifiedDate;

	private AdvertisingCampaignEntity advertisingCampaign;
	private VideoListTypeEntity videoListType;
	private Collection<ItemEntity> items;
	private Collection<MediaChannelEntity> mediaChannels;

	@Id
	@SequenceGenerator(name = "seq_mediaplan", sequenceName = "seq_mediaplan", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_mediaplan")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", length = 128, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "start_date", nullable = false)
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(DateTime startDate) {
		this.startDate = startDate;
	}

	@Column(name = "end_date", nullable = false)
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(DateTime endDate) {
		this.endDate = endDate;
	}

	@Column(name = "payment_date")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(DateTime paymentDate) {
		this.paymentDate = paymentDate;
	}

	@CreatedBy
	@Column(name = "created_by", length = 128)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@CreatedDate
	@Column(name = "created_date")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}

	@LastModifiedBy
	@Column(name = "last_modified_by", length = 128)
	public String getLastModifiedBy() {
		return this.lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	@LastModifiedDate
	@Column(name = "last_modified_date")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	public DateTime getLastModifiedDate() {
		return this.lastModifiedDate;
	}

	public void setLastModifiedDate(DateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	@ManyToOne
	@JoinColumn(name = "advertising_campaign", nullable = false)
	public AdvertisingCampaignEntity getAdvertisingCampaign() {
		return advertisingCampaign;
	}

	public void setAdvertisingCampaign(AdvertisingCampaignEntity advertisingCampaign) {
		this.advertisingCampaign = advertisingCampaign;
	}

	@ManyToOne
	@JoinColumn(name = "videoListType", nullable = false)
	public VideoListTypeEntity getVideoListType() {
		return videoListType;
	}

	public void setVideoListType(VideoListTypeEntity videoListType) {
		this.videoListType = videoListType;
	}

	@OneToMany(mappedBy = "mediaplan", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Collection<ItemEntity> getItems() {
		return items;
	}

	public void setItems(Collection<ItemEntity> items) {
		this.items = items;
	}

	@ManyToMany
	@JoinTable(name = "mediaplan_channel", joinColumns = @JoinColumn(name = "mediaplan_id"), inverseJoinColumns = @JoinColumn(name = "media_channel_id"))
	public Collection<MediaChannelEntity> getMediaChannels() {
		return mediaChannels;
	}

	public void setMediaChannels(Collection<MediaChannelEntity> mediaChannels) {
		this.mediaChannels = mediaChannels;
	}

	@JsonIgnore
	@Transient
	public boolean isNew() {
		if(this.id == null) {
			return true;
		}
		return false;
	}
}