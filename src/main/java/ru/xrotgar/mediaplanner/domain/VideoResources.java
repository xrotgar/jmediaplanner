package ru.xrotgar.mediaplanner.domain;

import java.io.Serializable;
import java.util.List;

public class VideoResources implements Serializable {

	private List<VideoResourceEntity> videoResources;

	public VideoResources() { }

	public VideoResources(List<VideoResourceEntity> videoResources) {
		this.videoResources = videoResources;
	}

	public List<VideoResourceEntity> getVideoResources() {
		return videoResources;
	}

	public void setVideoResources(List<VideoResourceEntity> videoResources) {
		this.videoResources = videoResources;
	}
}
