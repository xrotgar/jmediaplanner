package ru.xrotgar.mediaplanner.security.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import ru.xrotgar.mediaplanner.domain.UserEntity;
import ru.xrotgar.mediaplanner.repository.UserRepository;

@Repository("myUserDetailsService")
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository repository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity user = repository.findByLogin(username);
		if(user == null) {
			throw new UsernameNotFoundException("User " + username + " not found");
		}
		return new MyUserDetails(user);
	}
}
