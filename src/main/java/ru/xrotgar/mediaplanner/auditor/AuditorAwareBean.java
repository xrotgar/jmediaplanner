package ru.xrotgar.mediaplanner.auditor;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

@Repository("auditorAwareBean")
public class AuditorAwareBean implements AuditorAware<String> {
	public String getCurrentAuditor() {
		return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
}
