package ru.xrotgar.mediaplanner.controller.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.xrotgar.mediaplanner.domain.Companies;
import ru.xrotgar.mediaplanner.domain.CompanyEntity;
import ru.xrotgar.mediaplanner.service.CompanyService;

@RestController
@RequestMapping(value = "/company")
public class CompanyRestfulController {

	@Autowired
	private CompanyService service;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Companies list() {
		return new Companies(this.service.findAll());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public CompanyEntity findById(@PathVariable Long id) {
		return this.service.findById(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public CompanyEntity create(@RequestBody CompanyEntity company) {
		return this.service.save(company);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void update(@RequestBody CompanyEntity company, @PathVariable Long id) {
		this.service.save(company);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		this.service.delete(id);
	}
}