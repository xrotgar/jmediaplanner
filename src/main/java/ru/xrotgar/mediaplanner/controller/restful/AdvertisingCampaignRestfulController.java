package ru.xrotgar.mediaplanner.controller.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.xrotgar.mediaplanner.domain.AdvertisingCampaignEntity;
import ru.xrotgar.mediaplanner.domain.AdvertisingCampaigns;
import ru.xrotgar.mediaplanner.service.AdvertisingCampaignService;

@RestController
@RequestMapping(value = "/advertising_campaign")
public class AdvertisingCampaignRestfulController {

	@Autowired
	private AdvertisingCampaignService service;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public AdvertisingCampaigns list() {
		return new AdvertisingCampaigns(this.service.findAll());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public AdvertisingCampaignEntity findById(@PathVariable Long id) {
		return this.service.findById(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public AdvertisingCampaignEntity create(@RequestBody AdvertisingCampaignEntity advertisingCampaign) {
		return this.service.save(advertisingCampaign);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void update(@RequestBody AdvertisingCampaignEntity advertisingCampaign, @PathVariable Long id) {
		this.service.save(advertisingCampaign);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		this.service.delete(id);
	}
}