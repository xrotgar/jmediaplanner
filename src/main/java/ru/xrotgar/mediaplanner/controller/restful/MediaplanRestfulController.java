package ru.xrotgar.mediaplanner.controller.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.xrotgar.mediaplanner.domain.MediaplanEntity;
import ru.xrotgar.mediaplanner.domain.Mediaplans;
import ru.xrotgar.mediaplanner.service.MediaplanService;

@RestController
@RequestMapping(value = "/mediaplan")
public class MediaplanRestfulController {

	@Autowired
	private MediaplanService service;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Mediaplans list() {
		return new Mediaplans(this.service.findAll());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public MediaplanEntity findById(@PathVariable Long id) {
		return this.service.findById(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public MediaplanEntity create(@RequestBody MediaplanEntity mediaplan) {
		return this.service.save(mediaplan);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void update(@RequestBody MediaplanEntity mediaplan, @PathVariable Long id) {
		this.service.save(mediaplan);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		this.service.delete(id);
	}
}