package ru.xrotgar.mediaplanner.controller.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.xrotgar.mediaplanner.domain.VideoListTypeEntity;
import ru.xrotgar.mediaplanner.domain.VideoListTypes;
import ru.xrotgar.mediaplanner.service.VideoListTypeService;

@RestController
@RequestMapping(value = "/video_list_type")
public class VideoListTypeRestfulController {

	@Autowired
	private VideoListTypeService service;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public VideoListTypes list() {
		return new VideoListTypes(this.service.findAll());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public VideoListTypeEntity findById(@PathVariable String id) {
		return this.service.findById(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public VideoListTypeEntity create(@RequestBody VideoListTypeEntity videoListType) {
		return this.service.save(videoListType);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void update(@RequestBody VideoListTypeEntity videoListType, @PathVariable String id) {
		this.service.save(videoListType);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable String id) {
		this.service.delete(id);
	}
}