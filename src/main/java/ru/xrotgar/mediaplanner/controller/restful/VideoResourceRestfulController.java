package ru.xrotgar.mediaplanner.controller.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.xrotgar.mediaplanner.domain.VideoResourceEntity;
import ru.xrotgar.mediaplanner.domain.VideoResources;
import ru.xrotgar.mediaplanner.service.VideoResourceService;

@RestController
@RequestMapping(value = "/video_resource")
public class VideoResourceRestfulController {

	@Autowired
	private VideoResourceService service;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public VideoResources list() {
		return new VideoResources(this.service.findAll());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public VideoResourceEntity findById(@PathVariable Long id) {
		return this.service.findById(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public VideoResourceEntity create(@RequestBody VideoResourceEntity videoResource) {
		return this.service.save(videoResource);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void update(@RequestBody VideoResourceEntity videoResource, @PathVariable Long id) {
		this.service.save(videoResource);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		this.service.delete(id);
	}
}