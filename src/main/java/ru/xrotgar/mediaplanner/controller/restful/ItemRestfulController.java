package ru.xrotgar.mediaplanner.controller.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.xrotgar.mediaplanner.domain.ItemEntity;
import ru.xrotgar.mediaplanner.domain.Items;
import ru.xrotgar.mediaplanner.service.ItemService;

@RestController
@RequestMapping("/item")
public class ItemRestfulController {

	@Autowired
	private ItemService service;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Items list() {
		return new Items(this.service.findAll());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ItemEntity findById(@PathVariable Long id) {
		return this.service.findById(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ItemEntity create(@RequestBody ItemEntity item) {
		return this.service.save(item);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void update(@RequestBody ItemEntity item, @PathVariable Long id) {
		this.service.save(item);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		this.service.delete(id);
	}
}