package ru.xrotgar.mediaplanner.controller.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.xrotgar.mediaplanner.domain.MediaChannelEntity;
import ru.xrotgar.mediaplanner.domain.MediaChannels;
import ru.xrotgar.mediaplanner.service.MediaChannelService;

@RestController
@RequestMapping(value = "/media_channel")
public class MediaChannelRestfulController {

	@Autowired
	private MediaChannelService service;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public MediaChannels list() {
		return new MediaChannels(this.service.findAll());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public MediaChannelEntity findById(@PathVariable Long id) {
		return this.service.findById(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public MediaChannelEntity create(@RequestBody MediaChannelEntity mediaChannel) {
		return this.service.save(mediaChannel);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void update(@RequestBody MediaChannelEntity mediaChannel, @PathVariable Long id) {
		this.service.save(mediaChannel);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		this.service.delete(id);
	}
}