package ru.xrotgar.mediaplanner.controller.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.xrotgar.mediaplanner.domain.UserEntity;
import ru.xrotgar.mediaplanner.domain.Users;
import ru.xrotgar.mediaplanner.service.UserService;

@RestController
@RequestMapping(value = "/user")
public class UserRestfulController {

	@Autowired
	private UserService service;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Users list() {
		return new Users(this.service.findAll());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public UserEntity findById(@PathVariable Long id) {
		return this.service.findById(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public UserEntity create(@RequestBody UserEntity user) {
		return this.service.save(user);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void update(@RequestBody UserEntity user, @PathVariable Long id) {
		this.service.save(user);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		this.service.delete(id);
	}
}
