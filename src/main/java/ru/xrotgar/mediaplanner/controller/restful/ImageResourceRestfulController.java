package ru.xrotgar.mediaplanner.controller.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.xrotgar.mediaplanner.domain.ImageResourceEntity;
import ru.xrotgar.mediaplanner.domain.ImageResources;
import ru.xrotgar.mediaplanner.service.ImageResourceService;

@RestController
@RequestMapping(value = "/image_resource")
public class ImageResourceRestfulController {

	@Autowired
	private ImageResourceService service;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ImageResources list() {
		return new ImageResources(this.service.findAll());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ImageResourceEntity findById(@PathVariable Long id) {
		return this.service.findById(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ImageResourceEntity create(@RequestBody ImageResourceEntity imageResource) {
		return this.service.save(imageResource);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void update(@RequestBody ImageResourceEntity imageResource, @PathVariable Long id) {
		this.service.save(imageResource);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		this.service.delete(id);
	}
}