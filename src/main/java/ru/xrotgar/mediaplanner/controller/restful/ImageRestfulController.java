package ru.xrotgar.mediaplanner.controller.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.xrotgar.mediaplanner.domain.ImageEntity;
import ru.xrotgar.mediaplanner.domain.Images;
import ru.xrotgar.mediaplanner.service.ImageService;

@RestController
@RequestMapping(value = "/image")
public class ImageRestfulController {

	@Autowired
	private ImageService service;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Images list() {
		return new Images(this.service.findAll());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ImageEntity findById(@PathVariable Long id) {
		return this.service.findById(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ImageEntity create(@RequestBody ImageEntity image) {
		return this.service.save(image);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void update(@RequestBody ImageEntity image, @PathVariable Long id) {
		this.service.save(image);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		this.service.delete(id);
	}
}